#include "Sparse"
#include "Dense"


VectorXd TriSup(MatrixXd T, Vector Xd b)
{
  VectorXd X;
  X.resize(T.cols());

  double s = 0;
  X[T.cols()-1] = b[T.cols()-1]/T(T.rows()-1,T.cols()-1);

  for(int i = T.cols()-2; i >= 0; i--)
  {
    s = 0;

    for(k = i+1; k < T.cols(); i++ )
    {
      s += T(i,k)*X[k];
    }
    X[i] = (b[i]-s)/T(i,i);
  }

  return X;
}
