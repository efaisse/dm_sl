#include "residu_minimum.h"
#include <iostream>
#include "Sparse"
#include "Dense"

using namespace Eigen;
using namespace std;

residu_minimum::residu_minimum(SparseMatrix<double> A, VectorXd x0, VectorXd b, double epsilon, int kmax)
: _A(A), _x0(x0), _b(b), _epsilon(epsilon), _kmax(kmax)
{}

  void residu_minimum::Initialisation()
  {
    _r = _b - _A*_x0;
    _beta = (_r.dot(_r))/(_r.dot(_r));
  }

  void residu_minimum::Boucle()
  {

    int k = 0;
    VectorXd z;
    double alpha;

    while((_beta > _epsilon) && (k <= _kmax))
      {
        z = _A*_r;
        alpha = (_r.dot(_r))/(z.dot(_r));
        _x0 = _x0 + alpha*_r;
        _r = _r - alpha*z;
        _beta = sqrt(_r.dot(_r));
        k += 1;
      }
  }

  void residu_minimum::Affichage()
  {
    cout << _x0 << endl;
    cout << " " << endl;
    cout << _A*_x0 << endl;
  }
