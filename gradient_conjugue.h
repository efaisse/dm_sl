#include "Sparse"
#include "Dense"


class Gradient_conjugue
{
  private :
    Eigen::SparseMatrix<double> _A;
    Eigen::VectorXd _x0, _r, _b, _p;
    double _beta;
    double _epsilon;
    int _kmax;


  public :
    Gradient_conjugue(Eigen::SparseMatrix<double> A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
    void Initialisation();
    void Boucle();
    void Affichage();

};
