#include "SolveurLineaire.h"
#include "ExternMatrix.h"
#include "Sparse"
#include "Dense"
#include <string>
#include <iostream>
#include <vector>

using namespace std;
using namespace Eigen;

int main()
{

  cout << "Choisissez la matrice A à utiliser" << endl;
  cout << "1 = Matrice du Laplacien 2D" << endl;
  cout << "2 = Matrice de la forme aIn + (Bn^T)(Bn)" << endl;
  cout << "3 = Matrice depuis un fichier" << endl;

  int UserMatrixChoice;
  cin >> UserMatrixChoice;

  VectorXd x0, b;
  SparseMatrix<double> A;
  double epsilon = 0.00001;
  int n;

  switch(UserMatrixChoice)
  {
    case 1:
    {
      cout << "Donner le nombre de points de discrétisation (A de taille n*n)." << endl;
      cin >> n;
      A.resize(n*n,n*n);

      x0.resize(n*n); b.resize(n*n);
      x0.fill(-4.) ; b.fill(1.);

      vector<Triplet<double>> triplets;

      for (int i=0; i<A.rows(); i++) {
        triplets.push_back({i,i,4.});
        if ((i > 0) && (i%n != 0) )
        {triplets.push_back({i,i-1,-1.});}
        if ((i < A.rows()-1) && ((i+1)%n !=0))
        {triplets.push_back({i,i+1,-1.});}
        if (i < n*(n-1))
        {triplets.push_back({i, n+i, -1});}
        if (i < n*(n-1)-1)
        {triplets.push_back({n+i, i, -1});}
      }

      A.setFromTriplets(triplets.begin(), triplets.end());

      A = A/(n*n);

      break;}


      case 2:{

        cout << "Donner une taille de matrice." << endl;
        cin >> n;

        MatrixXd B(n,n), C(n,n);

        x0.resize(n); b.resize(n);
        x0.fill(-4.) ; b.fill(1.);

        double val, alpha;
        alpha = 0.1;
        for(int i=0; i<n; i++)
        {
          for(int j=0; j<n; j++)
          {
            val = (rand() % 10001)/10000.;
            B(i,j) = val;
          }
        }
        C.setIdentity();
        C = alpha*C + B.transpose()*B;

        SolveurLineaire<MatrixXd>* GPO(0);
        SolveurLineaire<MatrixXd>* GC(0);
        SolveurLineaire<MatrixXd>* RM(0);

        GPO = new GradientPasOptimal<MatrixXd>(C, x0, b, 4*B.cols(), epsilon);
        GC = new GradientConjugue<MatrixXd>(C, x0, b, epsilon, B.cols());
        RM = new ResiduMinimum<MatrixXd>(C, x0, b, epsilon, 4*B.cols());

        cout << "----------------------------------------------" << endl;
        cout << " Gradient a pas optimal " << endl;
        GPO->Resolution();
        GPO->AfficherSol();
        GPO->NormeResidu("GPO_residu.txt");
        cout << "----------------------------------------------" << endl;
        cout << " Gradient conjugue " << endl;
        GC->Resolution();
        GC->AfficherSol();
        GC->NormeResidu("GC_residu.txt");
        cout << "----------------------------------------------" << endl;
        cout << " Residu minimum " << endl;
        RM->Resolution();
        RM->AfficherSol();
        RM->NormeResidu("RM_residu.txt");

        GPO->~SolveurLineaire();
        GC->~SolveurLineaire();
        RM->~SolveurLineaire();

        break;}


      case 3:{

        string file_name;

        int UserExternMatrixChoice;
        cout << "Choisir la matrice à utiliser : "<< endl;
        cout << "1 = BCSSTK16" << endl;
        cout << "2 = NOS6" << endl;
        cout << "3 = NOS7" << endl;
        cout << "4 = BCSSTK14" << endl;
        cin >> UserExternMatrixChoice;

        ExternMatrix* ExtMat(0);

        switch(UserExternMatrixChoice)
        {
          case 1:
          file_name = "bcsstk16.mtx";
          ExtMat = new ExternMatrix(file_name);
          ExtMat->ReadMtx();
          A = ExtMat->Get_A();
          break;

          case 2:
          file_name = "nos6.mtx";
          ExtMat = new ExternMatrix(file_name);
          ExtMat->ReadMtx();
          A = ExtMat->Get_A();
          break;

          case 3:
          file_name = "nos7.mtx";
          ExtMat = new ExternMatrix(file_name);
          ExtMat->ReadMtx();
          A = ExtMat->Get_A();
          break;

          case 4:
          file_name = "bcsstk14.mtx";
          ExtMat = new ExternMatrix(file_name);
          ExtMat->ReadMtx();
          A = ExtMat->Get_A();
          break;

        }

        x0.resize(A.rows()); b.resize(A.rows());
        x0.fill(-4.) ; b.fill(1.);

        delete ExtMat;
        break;}


      default:
      cerr << "Ce choix n'est pas disponible" << endl;
    }

if(UserMatrixChoice != 2) {

  int UserSolverChoice;
  cout << "Choisir le solveur : "<< endl;
  cout << "1 = Gradient Pas Optimal" << endl;
  cout << "2 = Gradient Conjugué" << endl;
  cout << "3 = Residu Minimum" << endl;
  cout << "4 = Residu Minimum preconditionné" << endl;
  cout << "5 = Gradient Pas Optimal preconditionné" << endl;
  cin >> UserSolverChoice;

  switch (UserSolverChoice)
  {
    case 1:{
    SolveurLineaire<SparseMatrix<double>>* GPO(0);
    GPO = new GradientPasOptimal<SparseMatrix<double>>(A, x0, b, 4*A.cols(), epsilon);
    cout << "----------------------------------------------" << endl;
    cout << " Gradient à pas optimal " << endl;
    GPO->Resolution();
    GPO->AfficherSol();
    GPO->NormeResidu("GPO_residu.txt");
    GPO->~SolveurLineaire();
    break;}

    case 2:{
    SolveurLineaire<SparseMatrix<double>>* GC(0);
    GC = new GradientConjugue<SparseMatrix<double>>(A, x0, b, epsilon, A.cols());
    cout << "----------------------------------------------" << endl;
    cout << " Gradient conjugue " << endl;
    GC->Resolution();
    GC->AfficherSol();
    GC->NormeResidu("GC_residu.txt");
    GC->~SolveurLineaire();
    break;}

    case 3:{
    SolveurLineaire<SparseMatrix<double>>* RM(0);
    RM = new ResiduMinimum<SparseMatrix<double>>(A, x0, b, epsilon, 4*A.cols());
    cout << "----------------------------------------------" << endl;
    cout << " Residu minimum " << endl;
    RM->Resolution();
    RM->AfficherSol();
    RM->NormeResidu("RM_residu.txt");
    RM->~SolveurLineaire();
    break;}

    case 4:{
    SolveurLineaire<SparseMatrix<double>>* RMPreCond(0);
    RMPreCond = new ResiduMinimumPrecond<SparseMatrix<double>>(A, x0, b, epsilon, 200);
    cout << "----------------------------------------------" << endl;
    cout << " Residu Minimum preconditionne" << endl;;
    RMPreCond->Resolution();
    RMPreCond->AfficherSol();
    RMPreCond->NormeResidu("RMPrecond_residu.txt");
    RMPreCond->~SolveurLineaire();
    break;}

    case 5:{
    SolveurLineaire<SparseMatrix<double>>* GPOPreCond(0);
    GPOPreCond = new GradientPasOptimalPrecond<SparseMatrix<double>>(A, x0, b, epsilon, 200);
    cout << "----------------------------------------------" << endl;
    cout << " Gradient Pas Optimal preconditionne" << endl;;
    GPOPreCond->Resolution();
    GPOPreCond->AfficherSol();
    GPOPreCond->NormeResidu("GPOPrecond_residu.txt");
    GPOPreCond->~SolveurLineaire();
    break;}

    default:
    cerr << "Ce choix n'est pas disponible" << endl;

  }




  }


    return 0;
  }
