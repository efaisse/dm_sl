CC = g++

CXX_FLAGS = -I Eigen/Eigen -std=c++11

PROG = run

SRC = programme.cc SolveurLineaire.cpp ExternMatrix.cpp

$(PROG) : $(SRC)
					$(CC) $(SRC) $(CXX_FLAGS) -o $(PROG)

all : $(PROG)

clean :
			rm -f *.o *~ $(PROG)
