#include "Sparse"
#include "Dense"


class residu_minimum
{
  private :
    Eigen::SparseMatrix<double> _A;
    Eigen::VectorXd _x0, _r, _b;
    double _beta;
    double _epsilon;
    int _kmax;


  public :
    residu_minimum(Eigen::SparseMatrix<double> A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
    void Initialisation();
    void Boucle();
    void Affichage();

};
