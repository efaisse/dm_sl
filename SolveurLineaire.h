#include "Sparse"
#include "Dense"
#include <string>

//On template pour pouvoir utiliser les fonctions pour des matrices denses ou creuses
template<class T>
class SolveurLineaire
{

protected:
  //Matrice du problème considéré
  T _A;
  //Vecteurs solution, résidu, membre de droite de l'équation et norme du résidu
  Eigen::VectorXd _x, _r, _b, _normres;
  //Critère d'arrêt
  double _epsi, _beta;
  //Nombre d'itérations maximales
  int _kmax, _keff;

public:
  //Constructeur par défaut
  SolveurLineaire();
  // Destructeur par défaut
  virtual ~SolveurLineaire();
  //Résout le système en fonction du solveur choisi
  virtual void Resolution() = 0;
  //Affiche la norme du résidu (b-Ax, avec x le dernier itéré)
  void AfficherSol();
  //Enregistre la norme du résidu à chaque itération dans un fichier
  void NormeResidu(std::string nom);

};

//Classe fille publique de SolveurLineaire
template<class T>
class GradientPasOptimal : public SolveurLineaire<T>
{

public:
  //Constructeur de la classe fille GradientPasOptimal
  GradientPasOptimal( T A, Eigen::VectorXd x0, Eigen::VectorXd b, int kmax, double epsi);
  //Résout le système
  void Resolution();

};

//Classe fille publique de SolveurLineaire
template<class T>
class GradientConjugue : public SolveurLineaire<T>
{

private:
  Eigen::VectorXd _p;

public:
  //Constructeur de la classe fille GradientConjugue
  GradientConjugue(T A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
  //Résout le système
  void Resolution();

};

//Classe fille publique de SolveurLineaire
template<class T>
class ResiduMinimum : public SolveurLineaire<T>
{

public:
  //Constructeur de la classe fille ResiduMinimum
  ResiduMinimum(T A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
  //Résout le système
  void Resolution();

};

//Classe fille publique de SolveurLineaire
template<class T>
class ResiduMinimumPrecond : public SolveurLineaire<T>
{

private:
  //Vecteurs pour l'algorithme du résidu minimum préconditionné
  Eigen::VectorXd _q, _w;
  //Choix du préconditionneur
  int _precond;
  //Matrices de préconditionnement
  Eigen::MatrixXd _M, _M1, _M2;

public:
  //Constructeur de la classe fille ResiduMinimumPrecond
  ResiduMinimumPrecond(T A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
  //Résout le système
  void Resolution();

};

//Classe fille publique de SolveurLineaire
template<class T>
class GradientPasOptimalPrecond : public SolveurLineaire<T>
{

private:
  //Choix du préconditionneur
  int _precond;
  //Matrices de préconditionnement
  Eigen::MatrixXd _M, _M1, _M2;

public:
  //Constructeur de la classe fille GradientPasOptimalPrecond
  GradientPasOptimalPrecond(T A, Eigen::VectorXd x0, Eigen::VectorXd b, double epsilon, int kmax);
  //Résout le système
  void Resolution();

};

//Fonctions de résolution directe de systèmes linéaires de type Mx=b
//Lorsque M est tirangulaire supérieure
Eigen::VectorXd TriSup(Eigen::MatrixXd T, Eigen::VectorXd b);
//Lorsque M est triangulaire inférieure
Eigen::VectorXd TriInf(Eigen::MatrixXd L, Eigen::VectorXd b);
