#include "SolveurLineaire.h"
#include "Dense"
#include "Sparse"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace Eigen;
using namespace std;

//Constructeur par défaut
template<class T>
SolveurLineaire<T>::SolveurLineaire()
{}

// Destructeur par défaut
template<class T>
SolveurLineaire<T>::~SolveurLineaire()
{}

//Constructeur pour la classe fille GradientPasOptimal
template<class T>
GradientPasOptimal<T>::GradientPasOptimal(T A, VectorXd x0, VectorXd b, int kmax, double epsi)
{
  this->_A = A;
  this->_x = x0;
  this->_b = b;
  this->_epsi = epsi;
  this->_kmax = kmax;
  this->_r = b - A*x0;
  this->_normres.resize(this->_kmax+1);
  this->_beta = this->_r.norm();
  this->_normres(0) = this->_beta;
}

//Constructeur pour la classe fille GradientConjugue
template<class T>
GradientConjugue<T>::GradientConjugue(T A, VectorXd x0, VectorXd b, double epsi, int kmax)
{
  this->_A = A;
  this->_x = x0;
  this->_b = b;
  this->_epsi = epsi;
  this->_kmax = kmax;
  this->_r = b - A*x0;
  this->_normres.resize(this->_kmax+1);
  this->_p = this->_r;
  this->_beta = this->_r.norm();
  this->_normres(0) = this->_beta;
}

//Constructeur pour la classe fille ResiduMinimum
template<class T>
ResiduMinimum<T>::ResiduMinimum(T A, VectorXd x0, VectorXd b, double epsi, int kmax)
{
  this->_A = A;
  this->_x = x0;
  this->_b = b;
  this->_epsi = epsi;
  this->_kmax = kmax;
  this->_r = b - A*x0;
  this->_beta = this->_r.norm();
  this->_normres.resize(this->_kmax+1);
  this->_normres(0) = this->_beta;
}

//Constructeur pour la classe fille ResiduMinimumPrecond
template<class T>
ResiduMinimumPrecond<T>::ResiduMinimumPrecond(T A, VectorXd x0, VectorXd b, double epsi, int kmax)
{
  this->_A = A;
  MatrixXd A_dense = A;
  this->_x = x0;
  this->_b = b;
  this->_epsi = epsi;
  this->_kmax = kmax;
  this->_r = b - A*x0;
  this->_q.resize(this->_r.size());
  this->_beta = this->_r.norm();
  this->_normres.resize(this->_kmax+1);
  this->_normres(0) = this->_beta;
  cout << "Choisir un préconditionneur : " << endl;
  cout << "1 = Diagonal " << endl;
  cout << "2 = Gauss-Seidel symétrique " << endl;
  cout << "3 = Cholesky incomplet " << endl;
  cin >> _precond;
  this->_M.resize(A_dense.rows(),A_dense.cols());
  this->_M.setZero();
  this->_M1.resize(A_dense.rows(),A_dense.cols());
  this->_M1.setZero();
  this->_M2.resize(A_dense.rows(),A_dense.cols());
  this->_M2.setZero();
  switch (_precond)//Initialisation de _M et _q
  {
    case 1:{//Préconditionnement diagonal
      //M contient la diagonale de A
      for(int i = 0; i < this->_M.rows(); i++)
      {
        this->_M(i,i) = A_dense(i,i);
      }
      //Initialisation de _q
      for(int i = 0; i < this->_q.size(); i++)
        {this->_q[i] = this->_r[i]/this->_M(i,i);
        }
      break;}

    case 2:{//Préconditionnement Gauss-Seidel symétrique
      //Soit A = D-E-F avec D sa diagonale, -E sa partie strictement inférieure
      //et -F sa partie strictement supérieure
      //M est le produit de deux matrice triangulaires inférieure (M1) et supérieure (M2)
      //M1 = D-E
      for (int i = 0; i < this->_M1.rows(); i++){
        for (int j = 0; j <= i; j++){
          this->_M1(i,j) = A_dense(i,j);}
      }
      //M2 = D^-1 * (D-F)
      for (int i = 0; i < this->_M2.rows(); i++){
        for (int j = i; j < A.cols(); j++){
          this->_M2(i,j) = A_dense(i,j)/A_dense(i,i);}
      }
      //Initialisation de q
      VectorXd q1;
      q1 = TriInf(this->_M1, this->_r);
      this->_q = TriSup(this->_M2, q1);
      break;}

    case 3:{//Préconditionnement Cholesky incomplet
      LLT<MatrixXd> lltOfA(A_dense); //Decomposition Cholesky de A
      this->_M = lltOfA.matrixL(); //Retourne la matrice L de la décomposition précédente
      //ici M correspond à la décomposition Cholesky de A, à cela près que les
      //coefficients nuls de A sont conservés dans M
      for(int i = 0; i < _M.rows(); i++)
      {
        for(int j = 0; j < i ; j++)
        {
          if(A_dense(i,j) == 0)
          {
            this->_M(i,j) = 0;
          }
        }
      }
      //Initialisationd de q
      VectorXd q1;
      q1 = TriInf(this->_M, this->_r);
      this->_q = TriSup(this->_M.transpose(), q1);
      break;}

    default:
    cerr << "Ce choix n'est pas disponible" << endl;
  }
}

//Constructeur pour la classe fille GradientPasOptimalPrecond
template<class T>
GradientPasOptimalPrecond<T>::GradientPasOptimalPrecond(T A, VectorXd x0, VectorXd b, double epsi, int kmax)
{
  this->_A = A;
  MatrixXd A_dense = A;
  this->_x = x0;
  this->_b = b;
  this->_epsi = epsi;
  this->_kmax = kmax;
  this->_r = b - A*x0;
  this->_normres.resize(this->_kmax+1);
  this->_beta = this->_r.norm();
  this->_normres(0) = this->_beta;

  cout << "Choisir un préconditionneur : " << endl;
  cout << "1 = Diagonal " << endl;
  cout << "2 = Gauss-Seidel symétrique " << endl;
  cout << "3 = Cholesky incomplet " << endl;
  cin >> _precond;
  this->_M.resize(A_dense.rows(),A_dense.cols());
  this->_M.setZero();
  this->_M1.resize(A_dense.rows(),A_dense.cols());
  this->_M1.setZero();
  this->_M2.resize(A_dense.rows(),A_dense.cols());
  this->_M2.setZero();
  switch (_precond)//Initialisation de _M et _q
  {
    case 1:{//Préconditionnement diagonal
      //M contient la diagonale de A
      for(int i = 0; i < this->_M.rows(); i++)
      {
        this->_M(i,i) = A_dense(i,i);
      }
      break;}

    case 2:{//Préconditionnement Gauss-Seidel symétrique
      //Soit A = D-E-F avec D sa diagonale, -E sa partie strictement inférieure
      //et -F sa partie strictement supérieure
      //M est le produit de deux matrice triangulaires inférieure (M1) et supérieure (M2)
      //M1 = D-E
      for (int i = 0; i < this->_M1.rows(); i++){
        for (int j = 0; j <= i; j++){
          this->_M1(i,j) = A_dense(i,j);}
      }
      //M2 = D^-1 * (D-F)
      for (int i = 0; i < this->_M2.rows(); i++){
        for (int j = i; j < A.cols(); j++){
          this->_M2(i,j) = A_dense(i,j)/A_dense(i,i);}
      }
      break;}

    case 3:{//Préconditionnement Cholesky incomplet
      LLT<MatrixXd> lltOfA(A_dense); //Decomposition Cholesky de A
      this->_M = lltOfA.matrixL(); //Retourne la matrice L de la décomposition précédente
      //ici M correspond à la décomposition Cholesky de A, à cela près que les
      //coefficients nuls de A sont conservés dans M
      for(int i = 0; i < _M.rows(); i++)
      {
        for(int j = 0; j < i ; j++)
        {
          if(A_dense(i,j) == 0)
          {
            this->_M(i,j) = 0;
          }
        }
      }
      break;}

    default:
    cerr << "Ce choix n'est pas disponible" << endl;
  }
}

//Algorithme du Gradient à pas optimal
template<class T>
void GradientPasOptimal<T>::Resolution()
{
  int k = 0 ;
  VectorXd z;
  double alpha;
  while ( ( this->_beta > this->_epsi )&&( k <= this->_kmax) )
  {
    z = this->_A*this->_r ;
    alpha = this->_r.dot(this->_r)/z.dot(this->_r) ;
    this->_x += alpha*this->_r ;
    this->_r -= alpha*z ;
    this->_keff = k;
    this->_beta = this->_r.norm();
    this->_normres(k) = this->_beta;
    k += 1 ;
  }
  if ( k > this->_kmax )
  cout << "Tolérence non atteinte : " << this->_r.dot(this->_r) << endl ;
}

//Algorithme du Gradient conjugué
template<class T>
void GradientConjugue<T>::Resolution()
{
  int k = 0;
  double alpha, gamma;
  VectorXd z(this->_A.cols()), r2(this->_A.cols());
  while((this->_beta > this->_epsi) && (k <= this->_kmax))
  {
    z = this->_A*this->_p;
    alpha = (this->_r.dot(this->_r))/(z.dot(this->_p));
    this->_x += alpha*this->_p;
    r2 = this->_r - alpha*z;
    gamma = (r2.dot(r2))/(this->_r.dot(this->_r));
    this->_beta = this->_r.norm();
    this->_r = r2;
    this->_p = this->_r + gamma*this->_p;
    this->_keff = k;
    this->_normres(k) = this->_beta;
    k += 1;
  }
  if ( k > this->_kmax )
  cout << "Tolérence non atteinte : " << this->_r.dot(this->_r) << endl ;
}

//Algorithme du Résidu minimum
template<class T>
void ResiduMinimum<T>::Resolution()
{
  int k = 0;
  VectorXd z(this->_A.rows());
  double alpha;
  while((this->_beta > this->_epsi) && (k <= this->_kmax))
  {
    z = this->_A*this->_r;
    alpha = (this->_r.dot(z))/(z.dot(z));
    this->_x += alpha*this->_r;
    this->_r = this->_r - alpha*z;
    this->_beta = this->_r.norm();
    this->_keff = k;
    this->_normres(k) = this->_beta;
    k += 1;
  }
  if ( k > this->_kmax )
  cout << "Tolérence non atteinte : " << this->_r.dot(this->_r) << endl ;
}

//Algorithme du Résidu minimum préconditionné à gauche
template<class T>
void ResiduMinimumPrecond<T>::Resolution()
{
  int k = 0;
  VectorXd z(this->_A.rows()), w(this->_A.rows()), q1(this->_A.rows());
  double alpha;
  while((this->_beta > this->_epsi) && (k <= this->_kmax))
  {
    w = this->_A*this->_q;
    //Resoudre Mz = w;
    switch(_precond)
      {
      case 1:{
      for(int i = 0; i < w.size(); i++)
        {z[i] = w[i]/this->_M(i,i);}
        break;}
      case 2:{
        q1 = TriInf(this->_M1, w);
        z = TriSup(this->_M2, q1);
        break;}

      case 3:{
        q1 = TriInf(this->_M, w);
        z = TriSup(this->_M.transpose(), q1);
        break;}

      }

    alpha = (this->_q.dot(z))/(z.dot(z));
    this->_x += alpha*this->_q;
    this->_r = this->_r - alpha*w;
    this->_q = this->_q - alpha*z;
    this->_beta = this->_r.norm();
    this->_keff = k;
    this->_normres(k) = this->_beta;
    k += 1;
  }
  if ( k > this->_kmax )
  cout << "Tolérence non atteinte : " << this->_r.dot(this->_r) << endl ;
}

template<class T>
void GradientPasOptimalPrecond<T>::Resolution()
{
  int k = 0 ;
  VectorXd z(this->_A.cols()), q1;
  double alpha;
  while ( ( this->_beta > this->_epsi )&&( k <= this->_kmax) )
  {
    //Resoudre Mz = r;
    switch(_precond)
      {
      case 1:{
      for(int i = 0; i < this->_r.size(); i++)
        {  z[i] = this->_r[i]/this->_M(i,i);}
        break;}
      case 2:{
        q1 = TriInf(this->_M1, this->_r);
        z = TriSup(this->_M2, q1);
        break;}

      case 3:{
        q1 = TriInf(this->_M, this->_r);
        z = TriSup(this->_M.transpose(), q1);
        break;}

      }
    alpha = (this->_r.dot(z))/((this->_A*z).dot(z));
    this->_x += alpha*this->_r ;
    this->_r -= alpha*z ;
    this->_keff = k;
    this->_beta = this->_r.norm();
    this->_normres(k) = this->_beta;
    k += 1 ;
  }
  if ( k > this->_kmax )
  cout << "Tolérence non atteinte : " << this->_r.dot(this->_r) << endl ;
}

//Affiche la norme u résidu (b-Ax, avec x le dernier itéré)
template<class T>
void SolveurLineaire<T>::AfficherSol()
{
  //cout << " " << endl;
  //cout << this->_x << endl;
  cout << " " << endl;
  cout << (this->_b - this->_A*this->_x).norm() << endl;
  cout << " " << endl;
}


//Enregistre la norme du résidu à chaque itération dans un fichier
template<class T>
void SolveurLineaire<T>::NormeResidu(string nom)
{
  ofstream flux;
  flux.open(nom);
  for(int i = 0; i <= this->_keff; i++)
  {
    flux << i << " " << _normres(i) << endl;
  }
  flux.close();
}


template class SolveurLineaire<SparseMatrix<double>>;
template class SolveurLineaire<MatrixXd> ;
template class GradientPasOptimal<SparseMatrix<double>>;
template class GradientPasOptimal<MatrixXd> ;
template class ResiduMinimum<SparseMatrix<double>>;
template class ResiduMinimum<MatrixXd> ;
template class GradientConjugue<SparseMatrix<double>>;
template class GradientConjugue<MatrixXd> ;
template class ResiduMinimumPrecond<SparseMatrix<double>>;
template class ResiduMinimumPrecond<MatrixXd>;
template class GradientPasOptimalPrecond<SparseMatrix<double>>;
template class GradientPasOptimalPrecond<MatrixXd>;

//Fonctions de résolution directe de systèmes linéaires de type Mx=b
//Lorsque M est tirangulaire supérieure
VectorXd TriSup(MatrixXd T, VectorXd b)
{
  VectorXd X;
  X.resize(T.cols());

  double s = 0;
  X[T.cols()-1] = b[T.cols()-1]/T(T.rows()-1,T.cols()-1);
  for(int i = T.cols()-2; i >= 0; i--)
  {
    s = 0;
    for(int k = i+1; k < T.cols()-1; k++ )
    {
      s += T(i,k)*X[k];
    }
    X[i] = (b[i]-s)/T(i,i);
  }
  return X;
}

//Lorsque M est triangulaire inférieure
VectorXd TriInf(MatrixXd L, VectorXd b)
{
  VectorXd X1;
  X1.resize(L.cols());
  double s = 0;
  X1[0] = b[0]/L(0,0);

  for(int i = 1; i < L.rows(); i++)
  {
    s = 0;
    for(int k = 0; k < i; k++)
    {
      s += L(i,k)*X1[k];
    }
    X1[i] = (b[i]-s)/L(i,i);
  }
  return X1;
}
