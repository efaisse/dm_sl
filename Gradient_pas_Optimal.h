#include "Sparse"

class Gradient_pas_Optimal
{
  private :
    Eigen::SparseMatrix<double> _A;
    Eigen::VectorXd _x0, _r, _b;
    double _epsi;
    int _kmax;

  public :
    Gradient_pas_Optimal( Eigen::SparseMatrix<double> A, Eigen::VectorXd X, Eigen::VectorXd b, int kmax, double epsi);
    void Initialisation();
    void Boucle();
    void Affichage();

};
