#include "Gradient_pas_Optimal.h"
#include "Sparse"
#include <iostream>


using namespace std;
using namespace Eigen;

Gradient_pas_Optimal::Gradient_pas_Optimal(SparseMatrix<double> A, VectorXd X,VectorXd b,int kmax,double epsi):
_A(A), _x0(X), _b(b), _epsi(epsi), _kmax(kmax)
{}

  void Gradient_pas_Optimal::Initialisation()
  {
    _r = _b - _A*_x0;
  }

  void Gradient_pas_Optimal::Boucle()
{
  int k = 0 ;
  VectorXd z;
  double alpha;
  while ( ( _r.dot(_r) > _epsi )&&( k <= _kmax) )
  {
    z = _A*_r ;
    alpha = _r.dot(_r)/z.dot(_r) ;
    _x0 += alpha*_r ;
    _r -= alpha*z ;
    k += 1 ;
  }
  if ( k > _kmax )
  cout << "Tolérence non atteinte : " << _r.dot(_r) << endl ;
}

void Gradient_pas_Optimal::Affichage()
{
  cout << _x0 << endl;
  cout << " " << endl;
  cout << _A*_x0 << endl;
}
