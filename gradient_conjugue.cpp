#include "gradient_conjugue.h"
#include <iostream>
#include "Sparse"
#include "Dense"

using namespace Eigen;
using namespace std;

Gradient_conjugue::Gradient_conjugue(SparseMatrix<double> A, VectorXd x0, VectorXd b, double epsilon, int kmax)
: _A(A), _x0(x0), _b(b), _epsilon(epsilon), _kmax(kmax)
{}

void Gradient_conjugue::Initialisation()
{
  _r = _b - _A*_x0;
  _p = _r;
  _beta = sqrt(_r.dot(_r));
}

void Gradient_conjugue::Boucle()
{

  int k = 0;
  double alpha;
  VectorXd z, r2;
  while((_beta > _epsilon) && (k <= _kmax))
    {
      z = _A*_p;
      alpha = (_r.dot(_r))/(z.dot(_p));
      _x0 = _x0 + alpha*_p;
      r2 = _r - alpha*z;
      alpha = (r2.dot(r2))/(_r.dot(_r));
      _beta = sqrt(_r.dot(_r));
      _r = r2;
      _p = _r + alpha*_p;
      k += 1;
    }
}

void Gradient_conjugue::Affichage()
{
  cout << _x0 << endl;
  cout << " " << endl;
  cout << _A*_x0 << endl;
}
