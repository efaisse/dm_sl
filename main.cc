#include "gradient_conjugue.h"
#include "Gradient_pas_Optimal.h"
#include "residu_minimum.h"
#include "Sparse"
#include "Dense"
#include <iostream>
#include <vector>

using namespace std;
using namespace Eigen;

int main(){
SparseMatrix<double> A(3,3);
vector<Triplet<double>> triplets;

for (int i=0; i<A.rows(); i++) {
  triplets.push_back({i,i,2.});
  if (i > 0)
{triplets.push_back({i,i-1,-1.});}
  if (i < A.rows()-1)
  {triplets.push_back({i,i+1,-1.});}
}
A.setFromTriplets(triplets.begin(), triplets.end());

VectorXd x0, b;
double epsilon = 0.001;
int kmax = 10000;
x0.resize(3); b.resize(3);
x0.fill(-34.) ; b.fill(1.);

Gradient_conjugue GradConj(A, x0, b, epsilon, kmax);
GradConj.Initialisation();
GradConj.Boucle();
GradConj.Affichage();


x0.fill(-34.) ; b.fill(1.);

Gradient_pas_Optimal GPO(A, x0, b, kmax, epsilon);
GPO.Initialisation();
GPO.Boucle();
GPO.Affichage();

residu_minimum RM(A, x0, b, epsilon, kmax);
RM.Initialisation();
RM.Boucle();
RM.Affichage();

return 0;
}
