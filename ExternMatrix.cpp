#include "ExternMatrix.h"
#include "Dense"
#include "Sparse"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;
using namespace Eigen;

//Constructeur par défaut
ExternMatrix::ExternMatrix(string name): _file_name(name)
{}

//Lis un fichier .mtx pour remplir la matrice du système
void ExternMatrix::ReadMtx()
{
  ifstream flux(_file_name);

  string str;
  getline(flux, str);
  getline(flux, str);
  int matrixsize = stoi(str);

  _A.resize(matrixsize,matrixsize);
  vector<Triplet<double>> triplets;

  int i,j;
  double val;

  while (flux >> i >> j >> val)
 {
   triplets.push_back({i-1,j-1,val});
   if (i != j)
    {triplets.push_back({j-1,i-1,val});}
 }

 _A.setFromTriplets(triplets.begin(), triplets.end());
}

//Récupérer la matrice A
SparseMatrix<double> ExternMatrix::Get_A()
{
  return _A;
}
