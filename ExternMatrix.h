#include "Sparse"
#include "Dense"
#include <string>

class ExternMatrix
{

private:
  //Nom du fichier contenant les coefficients de la matrice
  std::string _file_name;
  //Matrice du système linéaire
  Eigen::SparseMatrix<double> _A;

public:
    //Constructeur
    ExternMatrix(std::string name);
    //Lis un fichier .mtx pour remplir la matrice du système
    void ReadMtx();
    //Récupérer la matrice A
    Eigen::SparseMatrix<double> Get_A();

};
